---
layout: post
title: 2024.1 - Tracker Solar
date: 2024-06-03 15:32:20 +0700
description: Tracker Solar para painéis fotovoltaicos
img: ![Imagem](https://lappis-unb.gitlab.io/fga-pi2/semestre-2024-1/grupo-03/docs/assets/images/promocad1.jpeg) # Add image post (optional)
fig-caption: # Add figcaption (optional)
tags: [Tracker Solar, energia fotovoltáica, energia solar, energia limpa, energia renovável]
comments: false
---

# Tracker Solar para painéis fotovoltaicos

**Problema a ser resolvido:** A crescente inserção de fontes de energia renováveis nas matrizes energéticas, impulsionada pela necessidade de mitigação de impactos ambientais e climáticos, caminha juntamente com o desenvolvimento de novas tecnologias e inovações.

Nesse contexto, a energia solar fotovoltaica é uma das fontes de energia renovável que vem sendo explorada e cada vez mais utilizada. Esta tecnologia transforma a luz solar em eletricidade por meio de células fotovoltaicas, oferecendo uma fonte de energia abundante e de baixa emissão de carbono. Apesar disto, um dos desafios enfrentados neste tipo de tecnologia é a sua baixa eficiência.

Surge, então, a ideia do "Tracker solar", um dispositivo mecânico e eletrônico projetado para orientar as placas solares fotovoltaicas através do rastreamento do movimento do sol durante o dia.

**Visão geral da solução**

O projeto visa criar um _tracker_ solar que otimize o desempenho das placas solares fotovoltaicas ao acompanhar o movimento do sol com um sistema de sensoriamento.
Além disso, fornece um sistema de notificações e registro de dados, fornecendo ao usuário
atualizações em tempo real sobre o status do _tracker_ e informações detalhadas
sobre seu funcionamento.

O objetivo do projeto é desenvolver um _tracker_ solar com baixo custo de
produção e alto desempenho na geração de energia. Além disso, busca-se integrar
um sistema de notificações que permita aos usuários acompanhar tanto problemas
técnicos quanto a produção do sistema ao longo do tempo.


**Poster**: Acesse o poster do projeto [aqui.](https://www.canva.com/design/DAGJRfyK6OA/OytSN_NJ7vaLpFZG7F_F8Q/view?utm_content=DAGJRfyK6OA&utm_campaign=designshare&utm_medium=link&utm_source=editor)

**Time** 

| Nome                                      | Matrícula | Curso                   |
| ----------------------------------------- | --------- | ----------------------- |
| Adrian Soares Lopes                       | 160000572 | Engenharia de Software  |
| Camila de Oliveira Borba                  | 170161722 | Engenharia Aeroespacial |
| Daniel Vinicius Ribeiro Alves             | 190026375 | Engenharia de Software  |
| Hugo Rocha de Moura                       | 180136925 | Engenharia de Software  |
| Ingryd Karine Batista Bruno               | 160008531 | Engenharia de Energia   |
| João Gabriel Antunes                      | 170013651 | Engenharia de Software  |
| João Paulo Lima da Silva                  | 190030755 | Engenharia de Software  |
| Jorge Guilherme Bezerra Amaral            | 150013485 | Engenharia Eletrônica   |
| José Joaquim da Silveira Araújo Junior    | 180123696 | Engenharia Eletrônica   |
| Marina da Matta Nery                      | 200062450 | Engenharia de Energia   |
| Pedro Henrique Guimarães de Souza Pereira | 190036486 | Engenharia Aeroespacial |
| Pedro Henrique Nogueira Bragança          | 190094478 | Engenharia de Software  |


**Fotos**
![imagem1](https://lappis-unb.gitlab.io/fga-pi2/semestre-2024-1/grupo-03/docs/assets/images/promocad1.jpeg)
![imagem2](https://lappis-unb.gitlab.io/fga-pi2/semestre-2024-1/grupo-03/docs/assets/images/promocad2.jpeg)
![imagem3](https://lappis-unb.gitlab.io/fga-pi2/semestre-2024-1/grupo-03/docs/assets/images/promoimg1.jpeg)
![imagem4](https://lappis-unb.gitlab.io/fga-pi2/semestre-2024-1/grupo-03/docs/assets/images/promoimg2.jpeg)

**Vídeos de funcionamento**

[Aqui](https://drive.google.com/drive/folders/17zSXN4J_19LfQKnHMbaTa-5jxgtNgvnR?usp=sharing)

**Repositório**: [Link para a solução](https://lappis-unb.gitlab.io/fga-pi2/semestre-2024-1/grupo-03/docs/software/landing.html)